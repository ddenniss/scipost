<!DOCTYPE html>
<html lang="ru">
  <head>
    <!-- %%(str "timestamp: " (timestamp))%% -->
    %%(define page (@id page-id PAGES))%%
    %%(standard-head-part
        #:title ($ title page)
        #:description ($ description page)
        #:keywords ($ keywords page)
        #:path-to-root ".."
    )%%

    %%(head-counters)%%

  </head>
  <body>
    <div class="container">
      %%(print-header #:path-to-root "..")%%
      %%(print-menu #:current-page ($ id page) #:section "меню_1" #:path-to-root "..")%%

      <div class="page">
        <div class="page_window">
          <h2>%%(str ($ title project))%%</h2>
        %%(format
            (dupstr "~a~n" 14)
            (show-section "Участник-заявитель" (format "~a ~a" ($ respondent_name project) ($ respondent_surname project)))
            (show-section "Город" ($ place project))
            (show-section "Контактная информация" ($ contacts project))
            (show-section "Статус проекта" ($ status project))
            (show-section-with-a "Сайт или страничка проекта в соцсетях" ($ url project))
            (show-section "Тематическая область" ($ topic project))
            (show-section "Краткое описание" ($ short_description project))
            (show-section "Подробное описание" ($ long_description project))
            (show-section "В каких ресурсах у проекта есть потребность" ($ resources_in_need project))
            (show-section "Какие ресурсы предлагаются другим проектам" ($ resources_to_suggest project))
            (show-section "Другие участники проекта" ($ participants project))
            (show-section "Похожие проекты" ($ similar_projects project))
            (show-section "Как проект способствует развитию технологий улучшения человеческого организма" ($ use_for_trabshumanism project))
            (show-section "Какие результаты должен принести (принес) проект по своему завершению" ($ project_artefacts project))
        )%%
        </div>
      </div>

    </div>
    %%(footer)%%
  </body>
</html>
