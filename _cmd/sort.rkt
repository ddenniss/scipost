 #lang racket

(require "../../_lib_links/odysseus_all.rkt")
(require "../../_lib_links/odysseus_tabtree.rkt")

(define file-to-sort "../knowledge/groups_ds.tree")

(define-namespace-anchor anchor)
(define ns-own (namespace-anchor->namespace anchor))

(tabtree-sort-and-print #:tabtree-file file-to-sort #:ns ns-own)
