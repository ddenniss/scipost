#lang racket

; sci_db database:sci_db_vk pwd:sci_db_vk

(require "../../_lib_links/odysseus_all.rkt")
(require "../../_lib_links/odysseus_report.rkt")
(require "../../_lib_links/odysseus_scrap.rkt")
(require "../../_lib_links/odysseus_tabtree.rkt")

(require "../_lib/functions.rkt")
(require "../_lib/scraping.rkt")
(require "../_lib/globals.rkt")

(persistent h-uid-fids-scraped)
(persistent h-uid-gids)

(define (->neo4j-path path)
  (string-replace path "c:" "//"))

; загружаем узлы с данными юзеров
(define-catch (add-user-nodes)
  (persistent h-uid-info-wo-rels)
  (let* (
        (h-uid-info-all (h-uid-info-wo-rels))
        (_ (--- (format "Всех пользователей: ~a" (length (hash-keys h-uid-info-all)))))
        (uid-info (filter-not
                          (λ (user)
                                    (or
                                      (equal? "DELETED" ($ name user))))
                          (hash-values h-uid-info-all)))
        (_ (--- (format "Действительных пользователей: ~a" (length uid-info))))

        (headers (list 'uid 'name 'surname 'bdate 'sex 'city 'interests 'university 'faculty 'job_position))
        (user_nodes_csv_command (format
                          "load csv from 'file:///user_nodes.csv' as line \
                          fieldterminator '\t' \
                          create (:User { uid: line.uid, name: line.name, surname: line.surname, bdate: line.bdate, sex: line.sex, city: line.city, interests: line.interests, university: line.university, faculty: line.faculty, job_position: line.job_position})"))
        (data (for/fold
                  ((res empty))
                  ((user uid-info))
                  (pushr res
                        (list
                          ($ uid user)
                          (or ($ name user) "")
                          (or ($ surname user) "")
                          (if ($ bdate user) (vk->date ($ bdate user)) "?")
                          (or ($ sex user) "?")
                          (or ($ city user) "?")
                          (format "\"~a\"" (text->csv-text (or ($ interests user) "?")))
                          (format "\"~a\"" (text->csv-text (or ($ university user) "?")))
                          (format "\"~a\"" (text->csv-text (or ($ faculty user) "?")))
                          (format "\"~a\"" (text->csv-text (or ($ job_position user) "?")))
                          ))))
        )
      (write-csv-file headers data (string-append CSV_PATH "/user_nodes.csv") #:delimeter "\t")
      (--- "загружаем узлы с данными юзеров")
      (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR user_nodes_csv_command))
      #t))

; загружаем связи между юзерами (френды)
; перед загрузкой проиндексировать uid: create index index_user_uid for (u:User) on (u.uid)
(define-catch (add-friend-rels)
  (let* (
        (h-uid-fids-scraped-snapshot (h-uid-fids-scraped))

        (headers (list 'uid1 'uid2))
        (data (car
                (for/fold
                  ; (car res) - список пар друзей, (cdr res) - список uid, для которых пары уже были составлены
                  ((res (cons empty empty)))
                  ((uid (hash-keys h-uid-fids-scraped-snapshot)))
                  (let* ((friend-ids (hash-ref h-uid-fids-scraped-snapshot uid empty))
                        (pairs (filter-map
                                  (λ (fid) (cond
                                              ((member fid (cdr res)) #f)
                                              (else
                                                (list uid fid))))
                                  friend-ids)))
                    (cons
                      (append (car res) pairs)
                      (pushr (cdr res) uid))))))
        (friends_csv_command (format
                          "load csv from 'file:///friends.csv' as line \
                          fieldterminator '\t' \
                          match (a:User), (b:User) \
                          where a.uid = line.uid1 and b.uid = line.uid2 \
                          create (a)-[:friend]->(b)"))
        )
    (write-csv-file headers data (string-append CSV_PATH "/friends.csv") #:delimeter "\t")
    (--- "загружаем связи между юзерами (френды)")
    (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR friends_csv_command))
    #t))

(define-catch (add-groups-hierarchy #:tree-files tree-files)
  (persistent h-galias-gid)
  (let* (
        (h-gid-galias (hash-revert (h-galias-gid)))
        (items (apply append
                (map
                  (λ (x) (planarize (parse-tab-tree (string-append "../knowledge/" x))))
                  tree-files)))
        (categories (filter group-category? items))
        (groups (filter sn-group? items))

        ; создаем узлы групп
        (headers1 (list 'gid 'gname))
        (data1 (for/fold
                  ((res empty))
                  ((g groups))
                  (let ((gid (hash-ref (h-galias-gid) (extract-pure-id ($ vk g)) #f))
                        (gname ($ id g)))
                    (if gid
                      (pushr res (list gid gname))
                      res))))
        (group_nodes_csv_command (format
                                    "load csv from 'file:///group_nodes.csv' as line \
                                    fieldterminator '\t' \
                                    create (:Group { gid: line[0], name: line[1]})"))

        ; создаем узлы категорий групп
        (headers2 (list 'gcname))
        (data2 (for/fold
                  ((res empty))
                  ((c categories))
                  (pushr res (list ($ id c)))))
        (category_nodes_csv_command (format
                                      "load csv from 'file:///category_nodes.csv' as line \
                                      fieldterminator '\t' \
                                      create (:GroupCategory { name: line[0]})"))

        ; создаем связи между группами и категориями
        (headers3 (list 'gid 'gcid))
        (data3 (for/fold
                  ((res empty))
                  ((g groups))
                  (pushr res (list ($ id g) ($ _parent g)))))
        (groups_to_categories_csv_command (format
                                          "load csv from 'file:///groups_to_categories.csv' as line \
                                          fieldterminator '\t' \
                                          match (g:Group), (gc:GroupCategory) \
                                          where g.name = line[0] and gc.name = line[1] \
                                          create (g)-[:category]->(gc)"))

        ; создаем связи между категориями
        (headers4 (list 'gcid1 'gcid2))
        (data4 (for/fold
                  ((res empty))
                  ((c categories))
                  (cond
                    (($ _parent c)
                        (pushr res (list ($ id c) ($ _parent c))))
                    (else
                      res))))
        (categories_to_categories_csv_command (format
                                            "load csv from 'file:///categories_to_categories.csv' as line \
                                            fieldterminator '\t' \
                                            match (gc1:GroupCategory), (gc2:GroupCategory) \
                                            where gc1.name = line[0] and gc2.name = line[1] \
                                            create (gc1)-[:category]->(gc2)"))
          )
    (write-csv-file headers1 data1 (string-append CSV_PATH "/group_nodes.csv") #:delimeter "\t")
    (write-csv-file headers2 data2 (string-append CSV_PATH "/category_nodes.csv") #:delimeter "\t")
    (write-csv-file headers3 data3 (string-append CSV_PATH "/groups_to_categories.csv") #:delimeter "\t")
    (write-csv-file headers4 data4 (string-append CSV_PATH "/categories_to_categories.csv") #:delimeter "\t")
    (--- "загружаем узлы групп")
    (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR group_nodes_csv_command))
    (--- "загружаем узлы категорий")
    (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR category_nodes_csv_command))
    (--- "загружаем связи типа группа-категория")
    (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR groups_to_categories_csv_command))
    (--- "загружаем связи типа категория-категория")
    (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR categories_to_categories_csv_command))
    #t))

; загружаем связи принадлежности юзеров к группам
(define-catch (add-group-member-rels)
  (persistent gids-scraped)
  (let* (
        (existed-gids (gids-scraped))
        (headers (list 'uid 'gid))
        (data (for/fold
                  ((res empty))
                  (((uid gids) (h-uid-gids)) (i (in-naturals 1)))
                  ; #:break (> i 100)
                  (let* (
                        (gids (map ->number gids))
                        (gids (intersect existed-gids gids)))
                    (display (format "~a " (length gids))) (flush-output)
                    ; (display (format "~a ~a~n" (length gids) (cartesian-product (list uid) gids))) (flush-output)
                    (append res (cartesian-product (list uid) gids)))))
        (_ (--- (length data)))
        (groups_csv_command (format
                          "load csv from 'file:///users_groups_rel.csv' as line \
                          fieldterminator '\t' \
                          match (u:User), (g:Group) \
                          where u.uid = line[0] and g.gid = line[1] \
                          create (u)-[:group_member]->(g)"))
        )
      (write-csv-file headers data (string-append CSV_PATH "/users_groups_rel.csv") #:delimeter "\t")
      (--- "загружаем связи принадлежности юзеров к группам")
      (system (format "~a/bin/cypher-shell.bat \"~a\"" NEO4J_DIR groups_csv_command))
      #t))

; (add-user-nodes)
; (add-friend-rels)
; (add-groups-hierarchy
;     #:tree-files (list
;                 "groups_ds.tree"
;                 "groups_edu.tree"
;                 "groups_rostov.tree"
;                 "groups_sci.tree"
;                 "groups_socium.tree"
;                 "groups_subcaucasus.tree"
;                 "groups_th.tree"
;                   ))
(add-group-member-rels)
